// Part 1
// let pictures = [];
// let words = ["one", "two"];

let pics = pictures;
let lines = words;

let numberOfPics = pics.length;
let numberOfLines = lines.length;

console.log("Number of Pictures: " + numberOfPics);
console.log("Number of Lines: " + numberOfLines);


// Part 2
let node1 = document.getElementById("paragraph-1-1");
let node2 = document.getElementById("paragraph-1-2");
node1.innerHTML = "Number of Pictures: " + numberOfPics;
node2.textContent = "Number of Lines: " + numberOfLines;